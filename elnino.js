d3.csv("elnino.csv", function (err, data) {
	if (!err) {
		handleData(data);
	}
});

function handleData(data) {
	var 	w = 1280,
		h = 720;

	var projection = d3.geo.mercator()
		.center([0, 5])
		.scale(900)
		.rotate([-180, 0]);

	var svg = d3.select('#plots').append('svg')
		.attr('width', w)
		.attr('height', h);

	var path = d3.geo.path()
		.projection(projection);

	var map = svg.append('g').attr('class', 'map');
	var humidities = svg.append('g').attr('class', 'humidities');
	var hubs = svg.append('g').attr('class', 'hubs');
	var winds = svg.append('g').attr('class', 'winds');
	
	var minHumidity, meanTemperature;

	var updateVisualization = function (data) {
		drawHubs(data);
		drawWinds(data);
	};

	d3.json("world-110m2.json", function (err, topology) {
		drawWorldMap(topology);

		d3.csv("elnino.csv", function (err, data) {
			minHumidity = d3.min(data, function (d) { return parseInt(d.humidity); });
			meanTemperature = d3.mean(data, function (d) { return parseInt(d.temperature); });	
			var day = 10;

			setInterval(function () {
				$('#day').html("Day: " + day);
				var currentData = _.filter(data, function (d) { return parseInt(d['day']) === day; });
				updateVisualization(currentData);
				day++;
				if (day > 14)
					day = 1;
			}, 1000);
		});
	});

	function filterByDay(day) {
		return function (d) {
			return parseInt(d['day']) === day;
		}
	}

	function drawWorldMap(topology) {
		hubs.selectAll('path')
			.data(topojson.object(topology, topology.objects.countries).geometries)
			.enter()
			.append('path')
			.attr('d', path);
	}

	function drawHubs(data) {
		var visualizeHubs = function (dataElems) {
			dataElems
				.attr('class', 'hub')
				.attr('cx', function (d) {
					return projection([d.lon, d.lat])[0];
				})
				.attr('cy', function (d) {
					return projection([d.lon, d.lat])[1];
				})
				.style('fill-opacity', 1.0);

			dataElems
				.transition()
				.duration(500)
				.attr('r', function (d) {
					var r = Math.round((d.humidity - minHumidity));

					if (isNaN(r))
						return 5;
					return r;
				})
				.style('fill', function (d) {
					var red = (127 + Math.round((d.temperature - meanTemperature) * 50)) || 0;
					var color = 'rgb(' + red + ', 0, 0)';
	
					return color;
				});
		};

		var circles = hubs.selectAll('circle').data(data, function (d) { return d.buoy; });

		visualizeHubs(circles);
		visualizeHubs(circles.enter().append('circle'));

		if ($('#hub').is(':checked'))
			circles.exit().remove();
		else
			circles.remove();
	}

	function drawWinds(data) {
		var visualizeWinds = function (dataElems, markers) {
			markers
				.attr('id', function (d) { return "marker" + d['buoy'];})
				.attr('viewBox', '0 0 10 10')
				.attr('refX', 1)
				.attr('refY', 3)
				.attr('markerWidth', 6)
				.attr('markerHeight', 6)
				.attr('orient', 'auto')
				.append('svg:path')
				.attr('d', 'M0,0L6,3L0,6 z')
				.attr('display', function (d) { return !isNaN(d['zonal']) && !isNaN(d['meridional']) ? 'auto' : 'none'; });

			dataElems
				.attr('class', 'wind')
				.attr('stroke', 'gray')
				.attr('stroke-width', '2')
				.attr('fill', 'none')
				.attr('marker-end', function (d) { return "url(#marker" + d['buoy'] + ")"; });

			dataElems
				.transition()
				.duration(1000)
				.attr('points', function (d) {
					var 	x1 = projection([d.lon, d.lat])[0],
						y1 = projection([d.lon, d.lat])[1],
						x2 = x1 + (d['zonal']*10 || 0),
						y2 = y1 - (d['meridional']*10 || 0);

					return x1 + "," + y1 + " " + x2 + "," + y2;
				});
		};
		var polylines = winds.selectAll('polyline').data(data, function (d) { return d.buoy; });
		var markers = winds.selectAll('marker').data(data, function (d) { return d.buoy; });

		visualizeWinds(polylines, markers);
		visualizeWinds(polylines.enter().append('polyline'), markers.enter().append('marker'));

		if ($('#wind').is(':checked')) {
			polylines.exit().remove();
			markers.exit().remove();
		}
		else {
			polylines.remove();
			markers.remove();
		}
	};

	var zoom = d3.behavior.zoom()
	    	.on("zoom", function() {
			var transform = function (group) {
				group.attr("transform","translate("+ 
			    		d3.event.translate.join(",")+")scale("+d3.event.scale+")");
			};
			transform(hubs);
			transform(winds);
		});

	svg.call(zoom);
}
