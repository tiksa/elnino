# elnino

A D3.js experiment. Visualizes differents properties of buoys in Pacific Ocean during two weeks with animations.

## Running

Just start a local server, for example `python -m SimpleHTTPServer`, and go to localhost:8000/elnino.html

## Screenshots

You can't see cool animations though :(

![elnino](screenshot.png)


